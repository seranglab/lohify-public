#include "io.hpp"
#include "Clock.hpp"

#include "LargestLayerLOH.hpp"
#include "MedianPivotLOH.hpp"
#include "OptimalLOH.hpp"
#include "QuickLayerOrderedHeap.hpp"

using namespace serang_lab;

double randreal_0_1() {
  constexpr int BIGINT = 1ul<<30;
  return (rand() % BIGINT) / double(BIGINT-1);
}

std::pair<double, bool> sample() {
  double score = randreal_0_1();
  score = score*score;

  
  double prob_is_tp = pow(score,4);

  bool is_tp = randreal_0_1()<prob_is_tp;
  return {score, is_tp};
}

double get_fdr(long tps, long fps) {
  return fps / double(tps);
}

std::pair<long,long> find_first_tps_fps_to_exceed_fdr_threshold_sort(std::pair<double,bool>*score_and_tp_s, unsigned long n, double fdr_threshold) {
  // sort by score in descending order:
  std::sort(score_and_tp_s, score_and_tp_s+n, [](auto lhs, auto rhs){return lhs>=rhs;});
  
  long num_tps=0, num_fps=0;
  
  for (unsigned long i=0; i<n; ++i) {
    bool is_tp = score_and_tp_s[i].second;

    //printn(score_and_tp_s[i].first, is_tp, "->", num_tps, num_fps);

    num_tps += is_tp;
    num_fps += !is_tp;

    if (num_tps > 0) {
      double fdr = get_fdr(num_tps, num_fps);
      if (fdr > fdr_threshold)
	return {num_tps, num_fps};
    }
  }
  return {-1,-1};
}

std::pair<long,long> ll_find_first_tps_fps_to_exceed_fdr_threshold_loh(std::pair<double,bool>*score_and_tp_s_begin, std::pair<double,bool>*score_and_tp_s_end, double fdr_threshold, LayerArithmetic*__restrict la, long num_tps=0, long num_fps=0) {
  unsigned long n=score_and_tp_s_end - score_and_tp_s_begin;
  LargestLayerLOH<std::pair<double,bool> > loh(score_and_tp_s_begin, n, la, [](auto lhs, auto rhs){return lhs>rhs;});

  for (unsigned long i=0; i<loh.n_layers(); ++i) {
    std::pair<double, bool>* layer_begin = loh.layer_begin(i);
    std::pair<double, bool>* layer_end = loh.layer_end(i);

    // count tps in layer
    long num_tps_in_layer = 0;
    for (auto iter=layer_begin; iter != layer_end; ++iter)
      num_tps_in_layer += iter->second;
    long num_fps_in_layer = loh.layer_size(i) - num_tps_in_layer;

    double fdr_optimistic = get_fdr(num_tps+num_tps_in_layer, num_fps);
    double fdr_pessemistic = get_fdr(num_tps, num_fps+num_fps_in_layer);

    if (fdr_threshold >= fdr_optimistic && fdr_threshold <= fdr_pessemistic) {
      
      // Note: could switch to sorting when problems are small enough.
      if (n>1) {
	// it's possible that the threshold is crossed in this layer:
	auto result = ll_find_first_tps_fps_to_exceed_fdr_threshold_loh(layer_begin, layer_end, fdr_threshold, la, num_tps, num_fps);
	if (result != std::make_pair(-1L,-1L))
	  return result;
      }
      else {
	// threshold may be crossed in this layer; since there is only
	// one order by which 1 item can appear, the entire layer must
	// be used.
	return {num_tps+num_tps_in_layer, num_fps+num_fps_in_layer};
      }
    }
  
    num_tps += num_tps_in_layer;
    num_fps += num_fps_in_layer;
  }
  return {-1,-1};
}

std::pair<long,long> mp_find_first_tps_fps_to_exceed_fdr_threshold_loh(std::pair<double,bool>*score_and_tp_s_begin, std::pair<double,bool>*score_and_tp_s_end, double fdr_threshold, LayerArithmetic*__restrict la, long num_tps=0, long num_fps=0) {
  unsigned long n=score_and_tp_s_end - score_and_tp_s_begin;
  MedianPivotLOH<std::pair<double,bool> > loh(score_and_tp_s_begin, n, la, [](auto lhs, auto rhs){return lhs>rhs;});

  for (unsigned long i=0; i<loh.n_layers(); ++i) {
    std::pair<double, bool>* layer_begin = loh.layer_begin(i);
    std::pair<double, bool>* layer_end = loh.layer_end(i);

    // count tps in layer
    long num_tps_in_layer = 0;
    for (auto iter=layer_begin; iter != layer_end; ++iter)
      num_tps_in_layer += iter->second;
    long num_fps_in_layer = loh.layer_size(i) - num_tps_in_layer;

    double fdr_optimistic = get_fdr(num_tps+num_tps_in_layer, num_fps);
    double fdr_pessemistic = get_fdr(num_tps, num_fps+num_fps_in_layer);

    if (fdr_threshold >= fdr_optimistic && fdr_threshold <= fdr_pessemistic) {
      
      // Note: could switch to sorting when problems are small enough.
      if (n>1) {
	// it's possible that the threshold is crossed in this layer:
	auto result = mp_find_first_tps_fps_to_exceed_fdr_threshold_loh(layer_begin, layer_end, fdr_threshold, la, num_tps, num_fps);
	if (result != std::make_pair(-1L,-1L))
	  return result;
      }
      else {
	// threshold may be crossed in this layer; since there is only
	// one order by which 1 item can appear, the entire layer must
	// be used.
	return {num_tps+num_tps_in_layer, num_fps+num_fps_in_layer};
      }
    }
  
    num_tps += num_tps_in_layer;
    num_fps += num_fps_in_layer;
  }
  return {-1,-1};
}

std::pair<long,long> ol_find_first_tps_fps_to_exceed_fdr_threshold_loh(std::pair<double,bool>*score_and_tp_s_begin, std::pair<double,bool>*score_and_tp_s_end, double fdr_threshold, LayerArithmetic*__restrict la, long num_tps=0, long num_fps=0) {
  unsigned long n=score_and_tp_s_end - score_and_tp_s_begin;
  OptimalLOH<std::pair<double,bool> > loh(score_and_tp_s_begin, n, la, [](auto lhs, auto rhs){return lhs>rhs;});

  for (unsigned long i=0; i<loh.n_layers(); ++i) {
    std::pair<double, bool>* layer_begin = loh.layer_begin(i);
    std::pair<double, bool>* layer_end = loh.layer_end(i);

    // count tps in layer
    long num_tps_in_layer = 0;
    for (auto iter=layer_begin; iter != layer_end; ++iter)
      num_tps_in_layer += iter->second;
    long num_fps_in_layer = loh.layer_size(i) - num_tps_in_layer;

    double fdr_optimistic = get_fdr(num_tps+num_tps_in_layer, num_fps);
    double fdr_pessemistic = get_fdr(num_tps, num_fps+num_fps_in_layer);

    if (fdr_threshold >= fdr_optimistic && fdr_threshold <= fdr_pessemistic) {
      
      // Note: could switch to sorting when problems are small enough.
      if (n>1) {
	// it's possible that the threshold is crossed in this layer:
	auto result = ol_find_first_tps_fps_to_exceed_fdr_threshold_loh(layer_begin, layer_end, fdr_threshold, la, num_tps, num_fps);
	if (result != std::make_pair(-1L,-1L))
	  return result;
      }
      else {
	// threshold may be crossed in this layer; since there is only
	// one order by which 1 item can appear, the entire layer must
	// be used.
	return {num_tps+num_tps_in_layer, num_fps+num_fps_in_layer};
      }
    }
  
    num_tps += num_tps_in_layer;
    num_fps += num_fps_in_layer;
  }
  return {-1,-1};
}

std::pair<long,long> ql_find_first_tps_fps_to_exceed_fdr_threshold_loh(std::pair<double,bool>*score_and_tp_s_begin, std::pair<double,bool>*score_and_tp_s_end, double fdr_threshold, long num_tps=0, long num_fps=0) {

  unsigned long n=score_and_tp_s_end - score_and_tp_s_begin;

  QuickLayerOrderedHeap<std::pair<double,bool> > loh(score_and_tp_s_begin, n, [](auto lhs, auto rhs){return lhs>rhs;});
  
  for (unsigned long i=0; i<loh.n_layers(); ++i) {
    std::pair<double, bool>* layer_begin = loh.layer_begin(i);
    std::pair<double, bool>* layer_end = loh.layer_end(i);

    // count tps in layer
    long num_tps_in_layer = 0;
    for (auto iter=layer_begin; iter != layer_end; ++iter)
      num_tps_in_layer += iter->second;
    long num_fps_in_layer = loh.layer_size(i) - num_tps_in_layer;

    double fdr_optimistic = get_fdr(num_tps+num_tps_in_layer, num_fps);
    double fdr_pessemistic = get_fdr(num_tps, num_fps+num_fps_in_layer);

    if (fdr_threshold >= fdr_optimistic && fdr_threshold <= fdr_pessemistic) {
      
      // Note: could switch to sorting when problems are small enough.
      if (n>1) {
	// it's possible that the threshold is crossed in this layer:
	auto result = ql_find_first_tps_fps_to_exceed_fdr_threshold_loh(layer_begin, layer_end, fdr_threshold, num_tps, num_fps);
	if (result != std::make_pair(-1L,-1L))
	  return result;
      }
      else {
	// threshold may be crossed in this layer; since there is only
	// one order by which 1 item can appear, the entire layer must
	// be used.
	return {num_tps+num_tps_in_layer, num_fps+num_fps_in_layer};
      }
    }
  
    num_tps += num_tps_in_layer;
    num_fps += num_fps_in_layer;
  }
  return {-1,-1};
}

void print_usage_error() {
  printn("Usage: <sort, slwgi, sdrpih, ppcca, quick, or all> <random_seed> <fdr_threshold> <n> <alpha (if needed)>");
  printn("'sort' is sorting.");
  printn("'slwgi' is selecting the layer with the greatest index.");
  printn("'sdrpih' is selecting to divide the remaining pivot indices in half.");
  printn("'ppcca' is partitioning on the pivot closest to the center of the array.");
  printn("'quick' is Quick-LOHify.");
  printn("Quick-LOHify generates its own partition indices, which are not determined by alpha;"
	,"and sorting does not require an alpha either.");
  exit(1);
}

int main(int argc, char**argv) {

  if (argc < 5) {
    print_usage_error();
  }

  std::string method(argv[1]);

  double alpha = 0.0;

  if (argc == 5) {
    if (method != "sort" && method != "quick")
      print_usage_error();
  }
  
  else if (argc == 6) {
    if (method != "slwgi" && method != "sdrpih"	&& method != "ppcca" && method != "all")
      print_usage_error();
    alpha = read<double>(argv[5]);
  }

  else {
    print_usage_error();
  }

  unsigned long seed = read<long>(argv[2]);
  double fdr_threshold = read<double>(argv[3]);
  unsigned long n = read<long>(argv[4]);
  
  std::pair<double, bool>*score_and_tp_s = new std::pair<double, bool>[n];

  if (method == "sort" || method == "all") {
    srand(seed);
    for (unsigned long i=0; i<n; ++i) score_and_tp_s[i] = sample();
    printn("Sorting:");
    Clock c;
    std::pair<long,long> result = find_first_tps_fps_to_exceed_fdr_threshold_sort(score_and_tp_s, n, fdr_threshold);
    c.ptock();
    printn("First true positive and first false positive to exceed fdr threshold :", result.first, ",", result.second, "\n");
  }

  if (method == "slwgi" || method == "all") {
    srand(seed);
    for (unsigned long i=0; i<n; ++i) score_and_tp_s[i] = sample();
    printn("LOH with selecting the layer with the greatest index:");
    Clock c;
    LayerArithmetic la(alpha);
    std::pair<long,long> result = ll_find_first_tps_fps_to_exceed_fdr_threshold_loh(score_and_tp_s, score_and_tp_s+n, fdr_threshold, &la);
    c.ptock();
    printn("First true positive and first false positive to exceed fdr threshold :", result.first, ",", result.second, "\n");
  }

  if (method == "sdrpih" || method == "all") {
    srand(seed);
    for (unsigned long i=0; i<n; ++i) score_and_tp_s[i] = sample();
    printn("LOH with selecting to divide the remaining pivot indices in half:");
    Clock c;
    LayerArithmetic la(alpha);
    std::pair<long,long> result = mp_find_first_tps_fps_to_exceed_fdr_threshold_loh(score_and_tp_s, score_and_tp_s+n, fdr_threshold, &la);
    c.ptock();
    printn("First true positive and first false positive to exceed fdr threshold :", result.first, ",", result.second, "\n");
  }

  if (method == "ppcca" || method == "all") {
    srand(seed);
    for (unsigned long i=0; i<n; ++i) score_and_tp_s[i] = sample();
    printn("LOH with partitioning on the pivot closest to the center of the array:");
    Clock c;
    LayerArithmetic la(alpha);
    std::pair<long,long> result = ol_find_first_tps_fps_to_exceed_fdr_threshold_loh(score_and_tp_s, score_and_tp_s+n, fdr_threshold, &la);
    c.ptock();
    printn("First true positive and first false positive to exceed fdr threshold :", result.first, ",", result.second, "\n");
  }

  if (method == "quick" || method == "all") {
    srand(seed);
    for (unsigned long i=0; i<n; ++i) score_and_tp_s[i] = sample();
    printn("Quick-LOHify:");
    Clock c;
    std::pair<long,long> result = ql_find_first_tps_fps_to_exceed_fdr_threshold_loh(score_and_tp_s, score_and_tp_s+n, fdr_threshold);
    c.ptock();
    printn("First true positive and first false positive to exceed fdr threshold :", result.first, ",", result.second, "\n");
  }
  
  return 0;
}
