## Layer-ordered heap example

Written in C++17, this is available to use subject to the MIT
license discussed in license.txt file.

An arxiv version of this paper is available at
https://arxiv.org/abs/2007.13356 

## Compile
Create executable using "make" in the /src/ directory

## USAGE
Executable will be called loh_fdr_example.

Usage:

Usage: <sort, slwgi, sdrpih, ppcca, quick, or all> <random_seed> <fdr_threshold> <n> <alpha (if needed)> 

Quick-LOHify generates its own partition indices, which are not determined by alpha; and sorting does not require an alpha either. 

**PARAMETERS:**

'sort' sorting. 
'slwgi' uses a LOH created by selecting the layer with the greatest index. 
'sdrpih' uses a LOH created by selecting to divide the remaining pivot indices in half. 
'ppcca' uses a LOH created by partitioning on the pivot closest to the center of the array. 
'quick' uses Quick-LOHify.
'all' performs all of them.

random_seed seeds the random number generator.
fdr_threshold is the threshold at which we return indices.
n is the number of values in our ranked list.

alpha determines the size of the layers in the layer ordered heaps,
alpha=1.0 means to sort everything so alpha>1.0 will increase.

For this particular example, alpha = 6 is good.

## EXAMPLES

**EXAMPLE USES**

./loh_fdr_example all 1 0.01 10000000 6

Notice we do not provide an alpha when we sort or use Quick-LOHify.

./loh_fdr_example sort 1 0.01 10000000
